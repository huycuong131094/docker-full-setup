### Centos 7 Only

Download the install script:

`curl -LJO https://gitlab.com/huycuong131094/docker-full-setup/-/raw/master/install.sh`

Run with sudo:

`sudo sh install.sh $(whoami)`

# Docker with git script

`curl -LJO https://gitlab.com/huycuong131094/docker-full-setup/-/raw/master/docker-git.sh`

Run with sudo:

`sudo sh docker-git.sh $(whoami)`
