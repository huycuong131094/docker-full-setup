#!/bin/sh
set -e

if [ -z "$1" ]
  then
    echo "No username supply! Try to install this script using current user by whoami. For example: sudo sh install.sh user1"
    exit 1
fi

yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine

yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce docker-ce-cli containerd.io

systemctl start docker
echo "------------------------ Start Docker Successfully ------------------------"

systemctl enable docker

echo "------------------------ Enable Docker Successfully ------------------------"

usermod -aG docker $1

echo "------------------------ Add Current User to Docker Group ------------------------"

echo "------------------------ Start Install Docker Compose ------------------------"

curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

echo "------------------------ Download Docker Compose complete ------------------------"

echo "------------------------ Add Execute Permission to docker-compose ------------------------"

chmod +x /usr/local/bin/docker-compose

echo "------------------------ Install Docker and Docker Compose Successfully ------------------------"
echo "------------------------ Logout and Login Again ------------------------"